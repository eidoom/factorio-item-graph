import mermaid from "./node_modules/mermaid/dist/mermaid.esm.min.mjs";

mermaid.initialize({
  arrowMarkerAbsolute: false,
  securityLevel: "loose",
  startOnLoad: true,
  theme: "neutral",

  flowchart: {
    htmlLabels: true,
    defaultRenderer: "elk",
  },
});
