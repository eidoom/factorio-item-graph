# factorio-item-graph

Live [here](https://eidoom.gitlab.io/factorio-item-graph).

Graph is incomplete.

## Local usage

Run on Fedora with, for example, 

```shell
sudo dnf install yarnpkg python3
cd ~/git/factorio-item-graph/public
yarn install
python3 -m http.server 8000
```

## Attribution

The thumbnails are from the [Factorio wiki](https://wiki.factorio.com/Factorio:Copyrights).

Raw data available [here](https://gist.githubusercontent.com/Bilka2/6b8a6a9e4a4ec779573ad703d03c1ae7/raw).

## Extra

Exploration of a Factorio calculator in `./calculator/`.
