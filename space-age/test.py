#!/usr/bin/env python3

import math

# module %s

## quality

QMS = 0.05  # flat speed reduction for all quality modules

QM1C = 0.01  # quality output chance
QM1U = 0.013
QM1R = 0.016
QM1E = 0.019
QM1L = 0.025

QM2C = 0.02
QM2U = 0.026
QM2R = 0.032
QM2E = 0.038
QM2L = 0.05

QM3C = 0.025
QM3U = 0.032
QM3R = 0.04
QM3E = 0.047
QM3L = 0.062

## productivity

PM1S = 0.05  # speed reduction
PM1C = 0.04  # productivity bonus per quality tier
PM1U = 0.05
PM1R = 0.06
PM1E = 0.07
PM1L = 0.1

PM2S = 0.1
PM2C = 0.06
PM2U = 0.07
PM2R = 0.09
PM2E = 0.11
PM2L = 0.15

PM3S = 0.15
PM3C = 0.1
PM3U = 0.13
PM3R = 0.16
PM3E = 0.19
PM3L = 0.25

# machines
# S = number of module slots
# P = base productivity bonus # note this can be affected per product by research
# EMP = electromagnetic plant
# F = foundry
# AM1 = assembling machine 1
# AM2 = assembling machine 2
# AM3 = assembling machine 3
# EF = electric furnace

EMPS = 5
EMPP = 0.5

FS = 4
FP = 0.5

AM1S = 0
AM1P = 0

AM2S = 2
AM2P = 0

AM3S = 4
AM3P = 0

EFS = 2
EFP = 0


def output(p, q, n):
    """
    quality:
    tiers: common, uncommon, rare, epic, legendary
    strengths: 0, 1, 2, 3, 5

    inputs:
    p: productivity bonus, eg 0.04 for 4%
    q: output quality chance, eg 0.04 for 4%
    n: If common inputs, it's the number of unlocked tiers,
       initially 3 then 4 and 5 unlocked by further research.
       If starting from higher quality inputs, then it's the number of steps up,
       eg with legendary unlocked and rare inputs, n = 2.
    """

    base = 1 + p

    probs = [base * (1 - q)]

    m = 1
    while len(probs) < n - 1:
        m /= 10
        probs.append(base * q * m * 9)

    probs.append(base * q * m)

    assert math.isclose(sum(probs), base)

    return probs


if __name__ == "__main__":
    # ignoring speed for now

    # print(EMPS * QM2R)
    # print(output(EMPP + 0 * PM2R, 5 * QM2R, 3))
    # print(output(EMPP + 1 * PM2R, 4 * QM2R, 3))
    # print(output(EMPP + 2 * PM2R, 3 * QM2R, 3))
    # print(output(EMPP + 3 * PM2R, 2 * QM2R, 3))
    # print(output(EMPP + 4 * PM2R, 1 * QM2R, 3))
    # print(output(EMPP + 5 * PM2R, 0 * QM2R, 3))

    print(output(EMPP + 0 * PM3L, 5 * QM3L, 5))
    print(output(EMPP + 1 * PM3L, 4 * QM3L, 5))
