BEGIN;

CREATE TABLE IF NOT EXISTS item (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL UNIQUE
) STRICT;

CREATE TABLE IF NOT EXISTS fluid (
	id INTEGER PRIMARY KEY REFERENCES item (id)
) STRICT;

CREATE TABLE IF NOT EXISTS fundamental (
	id INTEGER PRIMARY KEY REFERENCES item (id)
) STRICT;

CREATE TABLE IF NOT EXISTS recipe_category (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL UNIQUE
) STRICT;

CREATE TABLE IF NOT EXISTS recipe (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL UNIQUE,
	crafting_time REAL NOT NULL CHECK(crafting_time > 0.001),
	recipe_category_id INTEGER NOT NULL REFERENCES recipe_category (id)
) STRICT;

CREATE TABLE IF NOT EXISTS transput (
	id INTEGER PRIMARY KEY,
	amount INTEGER NOT NULL, -- -ve for ingredient, +ve for result
	recipe_id INTEGER NOT NULL REFERENCES recipe (id),
	item_id INTEGER NOT NULL REFERENCES item (id)
) STRICT;

CREATE TABLE IF NOT EXISTS machine (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL UNIQUE,
	full_power_draw INTEGER NOT NULL CHECK(full_power_draw >= 0), -- in kW
	-- is it sensible to have drain here when it only applies to electric energy source?
	drain REAL NOT NULL,
	crafting_speed REAL NOT NULL CHECK(crafting_speed >= 0),
	pollution REAL NOT NULL -- per second
) STRICT;

CREATE TABLE IF NOT EXISTS machine_for_recipe_category (
	id INTEGER PRIMARY KEY,
	machine_id INTEGER NOT NULL REFERENCES machine (id),
	recipe_category_id INTEGER NOT NULL REFERENCES recipe_category (id)
) STRICT;

-- choose the machine which should be used for each recipe category
-- should only be one per category
-- should be edited by the user to make the selection
CREATE TABLE active (
	id INTEGER PRIMARY KEY REFERENCES machine_for_recipe_category (id)
) STRICT;

COMMIT;
