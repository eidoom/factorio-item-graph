# calculator

Inspired by <http://kirkmcdonald.github.io/posts/calculation.html>.

## Data

Data in `script-output/` generated using [`--dump-data`](https://wiki.factorio.com/Command_line_parameters).
For example, on Windows for a Steam installation with [standard directories](https://wiki.factorio.com/Application_directory),

```powershell
cd "C:\Program Files (x86)\Steam\steamapps\common\Factorio"
.\bin\factorio.exe --dump-data
cd "C:\Users\[user name]\AppData\Roaming\Factorio"
ls script-output
```

or on Linux with Steam,

```shell
cd ~/.steam/steam/steamapps/common/Factorio
./bin/x64/factorio --dump-data
cd ~/.factorio
ls script-output
```

[API docs](https://lua-api.factorio.com/latest/index-prototype.html) useful (also in Factorio installation directory, view with `python3 -m http.server doc-html/`).

N.b. could also directly use the Lua files in `.../Factorio/data/base` (or available at [this repo](https://github.com/wube/factorio-data).

## Usage

Set outputs in `aims.json` then run script of your choice (each may have varying available features),

```shell
./*_calc.py
```

### Direct

`direct_calc.py` directly uses the outputted JSON for calculations, using a procedural paradigm.

### SQLite

Alternatively, `sql_calc.py` is set up to use SQLite.

The data dump is massaged into a SQLite3 database `factorio-data.db`

```shell
./json2db.py
```

which is tracked as the text file (dump with `./dump.sh`)

```shell
sqlite3 factorio-data.db .dump > factorio-data.sql
```

and can be restored to a database with

```shell
sqlite3 factorio-data.db ".read factorio-data.sql"
```

### Classes (most featureful)

`class_calc.py` introduces some classes to make the calculation easier to read.

## Notes

* beacons
* modules optimisation
* footprint as minimisation variable
* v2/Space Age
