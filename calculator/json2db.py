#!/usr/bin/env python3

import pathlib, sqlite3, json, argparse, time


def get_args():
    parser = argparse.ArgumentParser(
        description="Translate Factorio exported JSON data file into SQLite database"
    )
    di = "script-output/data-raw-dump.json"
    parser.add_argument(
        "-i",
        "--input",
        type=pathlib.Path,
        default=di,
        metavar="I",
        help=f"Path to JSON data file exported from Factorio, default {di}",
    )
    de = "extra.json"
    parser.add_argument(
        "-e",
        "--extra",
        type=pathlib.Path,
        default=de,
        metavar="E",
        help=f"Path to JSON data file with custom extra entries, default {de}",
    )
    do = "factorio-data.db"
    parser.add_argument(
        "-o",
        "--output",
        type=pathlib.Path,
        default=do,
        metavar="O",
        help=f"Path to output SQLite3 database, default {do}",
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="Overwrite database file if it already exists",
    )
    return parser.parse_args()


def check_existence(db, force):
    if force:
        db.unlink(missing_ok=True)

    elif db.is_file():
        raise FileExistsError(db)


def get_speed(machine):
    try:
        return machine["crafting_speed"]
    except KeyError:
        pass

    try:
        return machine["mining_speed"]
    except KeyError:
        pass

    try:
        # return machine["pumping_speed"]
        # ^ = 20, not sure why...
        # https://wiki.factorio.com/Offshore_pump
        return 1200
    except KeyError:
        pass


if __name__ == "__main__":
    args = get_args()

    check_existence(args.output, args.force)

    start = time.time()

    with open(args.input, "r") as f:
        raw = json.load(f)

    with open(args.extra, "r") as f:
        extra = json.load(f)

    ignore_cats = extra["ignore"]["categories"]

    cats = [x for x in raw["recipe-category"].keys() if x not in ignore_cats] + extra[
        "add"
    ]["categories"]

    machines = (
        raw["offshore-pump"]
        | raw["furnace"]
        | raw["assembling-machine"]
        | raw["mining-drill"]
        | raw["rocket-silo"]
        | extra["add"]["machines"]
    )

    fundamental = extra["add"]["recipes"]

    recipes = raw["recipe"] | fundamental

    items = {
        k: v
        for k, v in (
            raw["item"]
            | raw["fluid"]
            | raw["module"]
            | raw["tool"]  # science packs
            | raw["ammo"]
            | raw["gun"]
            | raw["armor"]
            | raw["capsule"]  # throwable items
            | raw["item-with-entity-data"]  # vehicles
            | raw["rail-planner"]  # for rail
            | raw["repair-tool"]
            | raw["spidertron-remote"]
        ).items()
        if k not in extra["ignore"]["items"]
    }

    con = sqlite3.connect(args.output)
    cur = con.cursor()
    cur.execute("PRAGMA foreign_keys = ON")

    with open("schema.sql", "r") as f:
        cur.executescript(f.read())

    cur.executemany(
        """
        INSERT INTO item (name) VALUES (?)
        """,
        [(x,) for x, _ in sorted(items.items(), key=lambda y: y[1]["order"])],
    )

    cur.executemany(
        """
        INSERT INTO fluid (id)
        VALUES ((SELECT id FROM item WHERE name = ?))
        """,
        [(k,) for k, v in items.items() if v["type"] == "fluid"],
    )

    cur.executemany(
        """
        INSERT INTO fundamental (id)
        VALUES ((SELECT id FROM item WHERE name = ?))
        """,
        [(x,) for x in fundamental.keys()],
    )

    cur.executemany(
        """
        INSERT INTO recipe_category (name) VALUES (?)
        """,
        [(x,) for x in cats],
    )

    cur.executemany(
        """
        INSERT INTO recipe (name, crafting_time, recipe_category_id)
        VALUES (?, ?, (SELECT id FROM recipe_category WHERE name = ?))
        """,
        [
            (
                k,
                v["energy_required"] if "energy_required" in v else 0.5,
                "crafting"
                if ("category" not in v or (c := v["category"]) in ignore_cats)
                else c,
            )
            for k, v in recipes.items()
        ],
    )

    for name, recipe in recipes.items():
        # TODO expensive mode
        if "normal" in recipe:
            recipe = recipe["normal"]

        if "result" in recipe:
            cur.execute(
                """
                INSERT INTO transput (amount, recipe_id, item_id)
                VALUES (
                    ?,
                    (SELECT id FROM recipe WHERE name = ?),
                    (SELECT id FROM item WHERE name = ?)
                )
                """,
                (
                    recipe["result_count"] if "result_count" in recipe else 1,
                    name,
                    recipe["result"],
                ),
            )

        for transput, sign in (("ingredients", -1), ("results", 1)):
            if transput in recipe:
                for component in recipe[transput]:
                    name_idx, amount_idx = (
                        ("name", "amount") if isinstance(component, dict) else (0, 1)
                    )
                    cur.execute(
                        """
                        INSERT INTO transput (amount, recipe_id, item_id)
                        VALUES (
                            ?,
                            (SELECT id FROM recipe WHERE name = ?),
                            (SELECT id FROM item WHERE name = ?)
                        )
                        """,
                        (sign * component[amount_idx], name, component[name_idx]),
                    )

    # TODO handle coal vs electric powder properly
    cur.executemany(
        """
        INSERT INTO machine (name, full_power_draw, drain, crafting_speed, pollution)
        VALUES (?, ?, ?, ?, ?)
        """,
        [
            (
                k,
                # power in kW
                (p := float(v["energy_usage"][:-2]) if "energy_usage" in v else 0),
                float(es["drain"][:-2])
                if "energy_source" in v and "drain" in (es := v["energy_source"])
                else (
                    0
                    if (
                        v["type"] in ("mining-drill", "rocket-silo")
                        or k in ("stone-furnace", "steel-furnace")
                    )
                    else p / 30
                ),
                get_speed(v),
                # pollution per second
                (
                    vv["emissions_per_minute"] / 60
                    if "emissions_per_minute" in (vv := v["energy_source"])
                    else 0
                )
                if "energy_source" in v
                else 0,
            )
            for k, v in machines.items()
        ],
    )

    for machine, details in machines.items():
        if "crafting_categories" in details:
            cur.executemany(
                """
                INSERT INTO machine_for_recipe_category (machine_id, recipe_category_id)
                VALUES (
                    (SELECT id FROM machine WHERE name = ?),
                    (SELECT id FROM recipe_category WHERE name = ?)
                )
                """,
                [
                    (machine, cat)
                    for cat in details["crafting_categories"]
                    if cat not in ignore_cats
                ],
            )
        else:
            match machine:
                case "pumpjack":
                    cat = "oil-extraction"
                case "offshore-pump":
                    cat = "water-extraction"
                case "manual":
                    cat = "manual"
                case _:
                    cat = "mining"

            cur.execute(
                """
                INSERT INTO machine_for_recipe_category (machine_id, recipe_category_id)
                VALUES (
                    (SELECT id FROM machine WHERE name = ?),
                    (SELECT id FROM recipe_category WHERE name = ?)
                )
                """,
                (machine, cat),
            )

    # nb only one active machine per category
    # we set default actives here
    cur.executemany(
        """
        INSERT INTO active (id)
        VALUES ((
            SELECT f.id
            FROM machine_for_recipe_category AS f
            INNER JOIN machine AS m ON m.id = f.machine_id
            INNER JOIN recipe_category AS c ON c.id = f.recipe_category_id
            WHERE m.name = ? AND c.name = ?
        ))
        """,
        [(m, c) for c, m in extra["default"]["active"].items()],
    )

    con.commit()
    con.close()

    print(f"Took {1000 * (time.time() - start):.0f}ms")
