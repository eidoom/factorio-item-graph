#!/usr/bin/env python3

import argparse
import collections
import copy
import json
import logging
import math
import pathlib
import sys
import time

import numpy
import scipy


# TODO
# add pumpjacks and offshore pumps
# better data format: py objects or sqlite db
# allow wastage if required
# ^ some runs will fail with no solution until fixed
# pipes/wagons/fluid wagon bandwidth
# allow selecting furnace/assembler/belt types
# modules and beacons


logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout)


def took(timer, title="took"):
    dur = 1000 * (time.time() - timer)
    logger.info(f"{title} {dur:.0f}ms")
    return dur


def component(item, recipe, field):
    if "normal" in recipe:
        recipe = recipe["normal"]

    plural = f"{field}s"
    if plural in recipe:
        return next(
            (
                r["amount" if d else 1]
                for r in recipe[plural]
                if r["name" if (d := isinstance(r, dict)) else 0] == item
            ),
            0,
        )

    if field == "result" and field in recipe and recipe[field] == item:
        return recipe["result_count"] if "result_count" in recipe else 1

    return 0


def get_energy_required(recipe):
    return recipe["energy_required"] if "energy_required" in recipe else 0.5


def format_power(p, u):
    assert u == "kW"

    if p > 1000:
        p /= 1000
        u = "MW"

    if p > 1000:
        p /= 1000
        u = "GW"

    return p, u


def get_args():
    parser = argparse.ArgumentParser(description="Factorio calculator")
    parser.add_argument(
        "-t",
        "--tax",
        action="store_true",
        help="Enable recipe tax",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        type=str,
        choices=("notset", "debug", "info", "warning", "error", "critical"),
        default="INFO",
        help="Set log level",
    )
    parser.add_argument(
        "-a",
        "--aims",
        type=pathlib.Path,
        default="aims.json",
        help="Set path for json file containing desired outputs in units of items per second",
    )
    parser.add_argument(
        "-d",
        "--decimal-places",
        type=int,
        default=2,
        help="Set number of decimal places in outputs",
    )
    return parser.parse_args()


if __name__ == "__main__":
    start = time.time()

    args = get_args()
    dp = args.decimal_places

    logger.setLevel(args.log_level.upper())

    logger.info("loading data")
    timer_data = time.time()

    with open("script-output/data-raw-dump.json", "r") as f:
        raw = json.load(f)

    furnace = raw["furnace"]["electric-furnace"]
    assembler = raw["assembling-machine"]["assembling-machine-1"]
    refinery = raw["assembling-machine"]["oil-refinery"]
    chemistry = raw["assembling-machine"]["chemical-plant"]
    miner = raw["mining-drill"]["electric-mining-drill"]
    # TODO others

    game_recipes = raw["recipe"]

    with open("extra.json", "r") as f:
        pseudos = json.load(f)["add"]["recipes"]

    recipes = game_recipes | pseudos

    logger.debug("recipes:")
    logger.debug(f"#={len(recipes)}")
    logger.debug([*recipes.keys()])

    items = (
        raw["item"]
        | raw["fluid"]
        | raw["module"]
        | raw["tool"]  # science packs
        | raw["ammo"]
        | raw["gun"]
        | raw["armor"]
        | raw["capsule"]  # throwable items
        | raw["item-with-entity-data"]  # vehicles
        | raw["rail-planner"]  # for rail
    )

    items = {
        k: v
        for k, v in items.items()
        if k
        # following items have no recipe to make them!
        not in (
            "item-unknown",
            "player-port",
            "coin",
            "heat-interface",
            "simple-entity-with-force",
            "simple-entity-with-owner",
            "infinity-chest",
            "infinity-pipe",
            "burner-generator",
            "linked-chest",
            "linked-belt",
            "fluid-unknown",
            "space-science-pack",  # should add recipe for this (item::satellite::rocket_launch_product)
            "tank-machine-gun",
            "tank-flamethrower",
            "tank-cannon",
            "artillery-wagon-cannon",
            "spidertron-rocket-launcher-1",
            "spidertron-rocket-launcher-2",
            "spidertron-rocket-launcher-3",
            "spidertron-rocket-launcher-4",
            "vehicle-machine-gun",
        )
    }

    logger.debug("items:")
    logger.debug(f"#={len(items)}")
    logger.debug([*items.keys()])

    with open(args.aims, "r") as f:
        aims_prio = json.load(f)

    aims = {k: v for k, v in aims_prio["aim"].items() if v}

    print("aims:")
    for aim, x in aims.items():
        print(aim, x)

    t_load = took(timer_data)

    logger.info("pre-processing data for optimisation")
    timer_pre = time.time()

    vector = numpy.array(
        [next((x for a, x in aims.items() if a == item), 0) for item in items.keys()]
    )

    def get_machine(recipe):
        return (
            {
                "mining": miner,
                "water-extraction": miner,  # TODO
                "oil-extraction": miner,  # TODO
                "manual": miner,  # TODO
                "smelting": furnace,
                "oil-processing": refinery,
                "chemistry": chemistry,
                "crafting": assembler,
                "advanced-crafting": assembler,
                "crafting-with-fluid": assembler,
                "rocket-building": assembler,  # TODO
                "centrifuging": assembler,  # TODO
            }[recipe["category"]]
            if "category" in recipe
            else assembler
        )

    def get_machine_crafting_speed(recipe):
        speed_field = (
            "mining_speed"
            if "category" in recipe
            and recipe["category"] in ("mining", "water-extraction", "oil-extraction", "manual")
            else "crafting_speed"
        )
        return get_machine(recipe)[speed_field]

    matrix = numpy.array(
        [
            [
                (
                    component(item, recipe, "result")
                    - component(item, recipe, "ingredient")
                )
                / get_energy_required(recipe)
                * get_machine_crafting_speed(recipe)
                for recipe in recipes.values()
            ]
            for item in items.keys()
        ]
    )

    # optionally introduce a "recipe tax":
    # a dummy cost on every (non-pseudo) recipe that is minimised
    # such that degenerate solutions using different sets of recipes
    # are broken to the one with the least recipes
    if args.tax:
        tax_row = numpy.array([1] * (len(game_recipes)) + [0] * len(pseudos))
        tax_col = numpy.array([0] * len(items) + [1])

        vector = numpy.append(vector, 0)

        matrix = numpy.vstack((matrix, tax_row))
        matrix = numpy.column_stack((matrix, tax_col))

    logger.debug("vector:")
    logger.debug(vector)
    logger.debug("matrix:")
    logger.debug(matrix)

    # assume critical ratio r will be less than ratio between largest and smallest absolute values in system
    a = numpy.abs(matrix)
    a = a[a != 0]
    r = a.max() / a.min()
    # then use r in objective function to rank input priorities

    # minimise all raw inputs
    # listed here by increasing priority (ie. minimise more the high priorities)
    priority = aims_prio["priority"]

    # bump up lowest power from 0 to 1 if using tax so tax can use 0
    root = 1 if args.tax else 0

    objective = [0] * (len(game_recipes)) + [
        r
        ** next(
            (i for i, names in enumerate(priority, start=root) if name in names), root
        )
        for name in pseudos.keys()
    ]

    if args.tax:
        objective += [1]

    logger.debug("objective:")
    logger.debug(objective)

    t_pre = took(timer_pre)

    logger.info("solving")
    timer_sol = time.time()

    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linprog.html
    sol = scipy.optimize.linprog(
        c=objective,
        A_ub=-matrix,
        b_ub=-vector,
    )

    logger.debug(sol)

    if not sol.success:
        raise Exception("Solver failed")

    t_solve = took(timer_sol)

    logger.info("post-processing result")
    timer_post = time.time()

    recipe_coeffs = {k: v for k, v in zip(recipes.keys(), sol.x) if v}

    logger.debug("recipe coefficients:")
    logger.debug(recipe_coeffs)

    result_coeffs = collections.defaultdict(int)
    for k, v in recipe_coeffs.items():
        r = recipes[k]
        m = v / get_energy_required(r) * get_machine_crafting_speed(r)
        if "normal" in r:
            r = r["normal"]
        if "results" in r:
            for res in r["results"]:
                rname = res["name" if isinstance(res, dict) else 0]
                result_coeffs[rname] += m * component(rname, r, "result")
        elif "result" in r:
            rname = r["result"]
            result_coeffs[rname] += m * component(rname, r, "result")
    result_coeffs = dict(result_coeffs)

    logger.debug("result coefficients:")
    logger.debug(result_coeffs)

    # TODO nb pumpjack and offshore pump data missing
    # could also calculate peak power and p7n
    # could calculate total buildings of each type required
    print("machines:")
    tot_pow = 0
    tot_p7n = 0
    for f, z in recipe_coeffs.items():
        m = get_machine(recipes[f])

        e = m["energy_usage"]
        eu = e[-2:]
        if eu != "kW":
            raise Exception(f"Unknown energy unit: {eu}")
        fe = float(e[:-2])
        x = fe * z
        if m["type"] != "mining-drill":
            if "energy_source" in m and "drain" in (es := m["energy_source"]):
                dr = es["drain"]
                du = dr[-2:]
                if du != "kW":
                    raise Exception(f"Unknown energy unit: {du}")
                df = float(dr[:-2])
            else:
                df = fe * math.ceil(z) / 30
            x += df
        tot_pow += x
        x, eu = format_power(x, eu)

        p7n = z * m["energy_source"]["emissions_per_minute"] / 60
        tot_p7n += p7n

        print(f"{m["name"]} > {f}: {z:.{dp}f} | {x:.{dp}f}{eu} | {p7n:.{dp}f} p7n")

    tot_pow, tot_u = format_power(tot_pow, "kW")
    print(f"total power: {tot_pow:.{dp}f}{tot_u}")
    print(f"total p7n: {tot_p7n:.{dp}f}")

    tbb = 15
    tu = "y-belts"

    print("inputs:")
    for r in pseudos.keys():
        if r in result_coeffs:
            n = result_coeffs[r]
            if items[r]["type"] == "item":
                ybs = n / tbb
                print(f"{r}: {n:.{dp}f} | {ybs:.{dp}f} {tu}")
                # TODO pipes
            else:
                print(f"{r}: {n:.{dp}f}")

    outputs = copy.deepcopy(result_coeffs)
    for n, a in recipe_coeffs.items():
        r = recipes[n]
        m = a / get_energy_required(r) * get_machine_crafting_speed(r)
        if "normal" in r:
            r = r["normal"]
        if "ingredients" in r:
            for ri in r["ingredients"]:
                kn, kx = ("name", "amount") if isinstance(ri, dict) else (0, 1)
                outputs[ri[kn]] -= m * ri[kx]

    intermediates = {
        k: yy
        for k, v in result_coeffs.items()
        if k not in pseudos.keys() and (yy := v - outputs[k])
    }

    if intermediates:
        print("intermediates:")
        for k, v in intermediates.items():
            if items[k]["type"] == "item":
                ybs = v / tbb
                print(f"{k}: {v:.{dp}f} | {ybs:.{dp}f} {tu}")
            else:
                print(f"{k}: {v:.{dp}f}")

    print("outputs:")
    for o, x in outputs.items():
        if not math.isclose(x, 0, abs_tol=1e-8):
            if items[o]["type"] == "item":
                ybs = x / tbb
                print(f"{o}: {x:.{dp}f} | {ybs:.{dp}f} {tu}")
            else:
                print(f"{o}: {x:.{dp}f}")

    t_post = took(timer_post)

    t_tot = took(start, "total:")

    logger.info("time distribution:")
    logger.info(
        f"load: {t_load/t_tot:.0%}, pre: {t_pre/t_tot:.0%}, solve: {t_solve/t_tot:.0%}, post: {t_post/t_tot:.0%}"
    )
