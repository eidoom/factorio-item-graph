#!/usr/bin/env python3

"""
Factorio calculator
"""

import argparse
import collections
import dataclasses
import json
import math
import pathlib
import pickle

import numpy
import scipy


def parse_unit(value, unit):
    n = len(unit)

    assert value[-n:] == unit

    p = value[-n - 1 : -n]

    return (
        float(value[:-n])
        if p.isdigit()
        else float(value[: -n - 1]) * 1e3 ** (1 + ["k", "M", "G"].index(p))
    )


@dataclasses.dataclass
class Base:
    name: str

    def __str__(self):
        return self.name


@dataclasses.dataclass
class Item(Base):
    subgroup: str


@dataclasses.dataclass
class Fuel(Item):
    fuel_value: float
    fuel_category: str


@dataclasses.dataclass
class Module(Item):
    """
    modules:
        speed
        effectivity
        productivity
    """

    effects: dict[str, int]
    allowed: list[str] | None

    def bonus(self, effect):
        """
        productivity
        consumption
        pollution
        speed
        """
        try:
            return self.effects[effect]
        except KeyError:
            return 0

    def allowed_recipe(self, recipe):
        """
        returns whether input recipe is in the list of allowed recipes for this module
        this is for productivity, which may only be used on intermediate products
        """
        return recipe in self.allowed if self.allowed is not None else True


def item_factory(data):
    name = data["name"]

    try:
        subgroup = data["subgroup"]
    except KeyError:
        subgroup = "other"

    if "fuel_value" in data:
        fuel_value = parse_unit(data["fuel_value"], "J")
        fuel_category = data["fuel_category"]
        return Fuel(name, subgroup, fuel_value, fuel_category)

    if "effect" in data:
        try:
            effects = {k: v["bonus"] for k, v in data["effect"].items()}
        except KeyError:
            effects = {}
        try:
            allowed = data["limitation"]
        except KeyError:
            allowed = None
        return Module(name, subgroup, effects, allowed)

    return Item(name, subgroup)


@dataclasses.dataclass
class Modules:
    modules: list[Module]

    def __len__(self):
        return len(self.modules)

    def bonus(self, effect):
        """
        nb effects stack additively
        """
        return sum(module.bonus(effect) for module in self.modules)

    def multipler(self, *effects, bound=-0.8):
        """
        multiple effects is for calculating pollution, which depends on pollution effect
        but also the consumption, which can be changed by the consumption effect
        these 2 effects stack multiplicatively
        """
        return max(bound, math.prod(1 + self.bonus(effect) for effect in effects))


@dataclasses.dataclass
class Machine(Base):
    base_crafting_speed: float
    energy_source: str
    base_full_pollution: float

    def __post_init__(self):
        self.full_pollution = self._full_pollution()
        self.crafting_speed = self._crafting_speed()

    def _crafting_speed(self):
        return self.base_crafting_speed

    def _full_pollution(self):
        """
        produced pollution units per minute at full usage
        """
        return self.base_full_pollution

    def pollution(self, u):
        """
        returns produced pollution units per second at machine utilisation u
        """
        return u * self.full_pollution


@dataclasses.dataclass
class ModuledMachine(Machine):
    modules_allowed: int
    modules: Modules
    allowed_effects: list[str]

    def _crafting_speed(self):
        return super()._crafting_speed() * self.modules.multipler("speed")

    def _full_pollution(self):
        """
        produced pollution units per minute at full usage
        """
        return super()._full_pollution() * self.modules.multipler(
            "pollution", "consumption"
        )


@dataclasses.dataclass
class ElectricMachine(Machine):
    # electric power used in W at full usage (doesn't include drain or module effects)
    basic_power_draw: float
    # nb modules don't affect drain
    power_drain: float

    def __post_init__(self):
        super().__post_init__()
        self.full_power_draw = self._full_power_draw()

    def _full_power_draw(self):
        """
        electric power used in W at full usage (doesn't include drain but does include modules)
        """
        return self.basic_power_draw

    def power_usage(self, u):
        """
        returns complete power used (draw + drain) when machine at utilisation u
        """
        return u * self.full_power_draw + math.ceil(u) * self.power_drain


@dataclasses.dataclass
class ModuledElectricMachine(ElectricMachine, ModuledMachine):
    def _full_power_draw(self):
        """
        electric power used in W at full usage (doesn't include drain but does include modules)
        """
        return super()._full_power_draw() * self.modules.multipler("consumption")


@dataclasses.dataclass
class BurnerMachine(Machine):
    fuel: Item
    energy_usage: float

    def __post_init__(self):
        super().__post_init__()
        # set number of fuel items consumed per second at full utilisation
        self.full_fuel_usage = self.energy_usage / self.fuel.fuel_value

    def fuel_usage(self, c):
        return self.full_fuel_usage * c


@dataclasses.dataclass
class Pumpjack(ModuledElectricMachine):
    field_yield: float

    def _crafting_speed(self):
        return super()._crafting_speed() * self.field_yield


class MachineFactory:
    def __init__(self, target, items):
        self.modules = (
            {
                k: [items[v] for v in vs]
                for k, vs in target["module"]["by-machine"].items()
            }
            if target["module"]["on"]
            else {}
        )

        self.fuel = items[target["fuel"]]

        # min: 0.2
        # max: 100
        self.field_yield = target["pumpjack-yield"]

    def match(self, data):
        name = data["name"]

        try:
            base_full_pollution = data["energy_source"]["emissions_per_minute"]
        except KeyError:
            base_full_pollution = 0

        try:
            energy_source = data["energy_source"]["type"]
        except KeyError:
            energy_source = None

        if energy_source == "electric":
            basic_power_draw = parse_unit(data["energy_usage"], "W")

            try:
                power_drain = parse_unit(data["energy_source"]["drain"], "W")
            except KeyError:
                power_drain = basic_power_draw / 30

        try:
            modules_allowed = data["module_specification"]["module_slots"]

            try:
                allowed_effects = data["allowed_effects"]
            except KeyError:
                if data["type"] == "mining-drill":
                    allowed_effects = [
                        "productivity",
                        "consumption",
                        "pollution",
                        "speed",
                    ]
                else:
                    allowed_effects = []

            try:
                modules = self.modules[name]
            except KeyError:
                modules = []

            modules = Modules(modules)

            if len(modules) > modules_allowed:
                print(f"Warning: {name} has too many modules")

            for m in modules.modules:
                if set(m.effects.keys()) - set(allowed_effects):
                    print(
                        f"Warning: module {m} in machine {name} has a non-allowed effect"
                    )

        except KeyError:
            pass

        match name:
            case "electric-mining-drill":
                base_crafting_speed = data["mining_speed"]
                return ModuledElectricMachine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    modules_allowed,
                    modules,
                    allowed_effects,
                    basic_power_draw,
                    power_drain,
                )
            case "assembling-machine-1":
                base_crafting_speed = data["crafting_speed"]
                return ElectricMachine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    basic_power_draw,
                    power_drain,
                )
            case (
                "assembling-machine-2"
                | "assembling-machine-3"
                | "centrifuge"
                | "chemical-plant"
                | "oil-refinery"
                | "electric-furnace"
            ):
                base_crafting_speed = data["crafting_speed"]
                return ModuledElectricMachine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    modules_allowed,
                    modules,
                    allowed_effects,
                    basic_power_draw,
                    power_drain,
                )
            case "lab":
                base_crafting_speed = data["researching_speed"]
                return ModuledElectricMachine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    modules_allowed,
                    modules,
                    allowed_effects,
                    basic_power_draw,
                    power_drain,
                )
            case "rocket-silo":
                base_crafting_speed = data["crafting_speed"]
                power_drain = 0
                return ModuledElectricMachine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    modules_allowed,
                    modules,
                    allowed_effects,
                    basic_power_draw,
                    power_drain,
                )
            case "burner-mining-drill":
                base_crafting_speed = data["mining_speed"]
                energy_usage = parse_unit(data["energy_usage"], "W")
                return BurnerMachine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    self.fuel,
                    energy_usage,
                )
            case "stone-furnace" | "steel-furnace":
                base_crafting_speed = data["crafting_speed"]
                energy_usage = parse_unit(data["energy_usage"], "W")
                return BurnerMachine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    self.fuel,
                    energy_usage,
                )
            case "boiler":
                base_crafting_speed = 1
                energy_usage = parse_unit(data["energy_consumption"], "W")
                return BurnerMachine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    self.fuel,
                    energy_usage,
                )
            case "pumpjack":
                # mining_speed = 1
                # 10 crude oil per field yield (* 10)
                base_crafting_speed = data["mining_speed"] * 10
                return Pumpjack(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                    modules_allowed,
                    modules,
                    allowed_effects,
                    basic_power_draw,
                    power_drain,
                    self.field_yield,
                )
            case "offshore-pump":
                base_crafting_speed = 1200
                return Machine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                )
            case "manual":
                base_crafting_speed = 1
                return Machine(
                    name,
                    base_crafting_speed,
                    energy_source,
                    base_full_pollution,
                )


class MachineSelector:
    def __init__(self, machines, actives):
        self.machines = machines
        self.actives = actives

    def match(self, crafting_category):
        if crafting_category in ("advanced-crafting", "basic-crafting"):
            crafting_category = "crafting"
        name = self.actives[crafting_category]
        return self.machines[name]


@dataclasses.dataclass
class Recipe(Base):
    ingredients: dict[str, float]
    results: dict[str, float]
    machine: Machine

    def ingredient(self, name):
        try:
            return self.ingredients[name]
        except KeyError:
            return 0

    def result(self, name):
        try:
            return self.results[name]
        except KeyError:
            return 0


def recipe_factory(data, machine_selector, mode="normal"):
    name = data["name"]

    try:
        crafting_category = data["category"]
    except KeyError:
        crafting_category = "crafting"

    machine = machine_selector.match(crafting_category)

    crafting_speed = machine.crafting_speed

    moded = data[mode] if mode in data else data

    try:
        crafting_time = moded["energy_required"]
    except KeyError:
        crafting_time = 0.5

    def transput(item):
        indices = ("name", "amount") if isinstance(item, dict) else (0, 1)
        return (item[x] for x in indices)

    def refine(items):
        # move this back into class if needs to be dynamic
        # but be careful not to call on fuel inputs
        return {k: v / crafting_time * crafting_speed for k, v in items.items()}

    try:
        ingredients = refine(dict(map(transput, moded["ingredients"])))
    except KeyError:
        ingredients = {}

    if machine.energy_source == "burner":
        used = machine.full_fuel_usage
        try:
            ingredients[machine.fuel.name] += used
        except KeyError:
            ingredients[machine.fuel.name] = used

    if "results" in moded:
        results = dict(map(transput, moded["results"]))
    elif "result" in moded:
        results = {
            moded["result"]: moded["result_count"] if "result_count" in moded else 1
        }

    if hasattr(machine, "modules"):
        results = {
            k: v * machine.modules.multipler("productivity", bound=0)
            for k, v in results.items()
        }

    results = refine(results)

    return Recipe(name, ingredients, results, machine)


def get_args():
    parser = argparse.ArgumentParser(description="Factorio calculator")
    return parser.parse_args()


def load_raw():
    raw_pickle = pathlib.Path("raw.pickle")
    raw_json = "script-output/data-raw-dump.json"

    if raw_pickle.is_file():
        with open(raw_pickle, "rb") as f:
            return pickle.load(f)

    with open(raw_json, "r") as f:
        raw = json.load(f)

    with open(raw_pickle, "wb") as f:
        pickle.dump(raw, f, pickle.HIGHEST_PROTOCOL)

    return raw


def format_power(p, dp):
    u = "W"

    if p > 1000:
        p /= 1000
        u = "kW"

    if p > 1000:
        p /= 1000
        u = "MW"

    if p > 1000:
        p /= 1000
        u = "GW"

    return f"{p:.{dp}f}{u}"


def items_and_recipes(extra, target):
    raw = load_raw()

    # things that can be transput
    items = {
        k: item_factory(v)
        for x in extra["include"]["item-list"]
        for k, v in raw[x].items()
        if k not in extra["ignore"]["items"]
    }

    machines = {
        k: v for x in extra["include"]["machine-list"] for k, v in raw[x].items()
    } | extra["add"]["machines"]

    machine_factory = MachineFactory(target, items)

    machines = {
        k: machine_factory.match(v)
        for k, v in machines.items()
        if k not in extra["ignore"]["machines"]
    }

    if any(len(m.modules) for m in machines.values() if hasattr(machines, "modules")):
        print("modules in use by machines:")
        for machine in machines.values():
            mods = [x.name for x in machine.modules.modules]
            if mods:
                print(f"{machine} ({machine.modules_allowed} max): {mods}")

    machine_selector = MachineSelector(
        machines, (extra["default"]["active"] | target["machine"])
    )

    recipes = (
        {k: v for k, v in raw["recipe"].items() if k not in extra["ignore"]["recipes"]}
        | extra["add"]["recipes"]
        | extra["add"]["recipes-other"]
    )

    recipes = {
        k: recipe_factory(v, machine_selector, target["mode"])
        for k, v in recipes.items()
    }

    return items, recipes


def get_vector(aims, items):
    return numpy.array([aims[item] if item in aims else 0 for item in items.keys()])


def get_matrix(items, recipes, power=False, pollution=False):
    matrix = numpy.array(
        [
            [
                recipe.result(item) - recipe.ingredient(item)
                for recipe in recipes.values()
            ]
            for item in items.keys()
        ]
    )

    if power:
        power_row = numpy.array(
            [r.power_usage() for r in recipes.values()]
            + [0] * (matrix.shape[1] - len(recipes))
        )
        power_col = numpy.array([0] * matrix.shape[0] + [1])

        vector = numpy.append(vector, 0)

        matrix = numpy.vstack((matrix, power_row))
        matrix = numpy.column_stack((matrix, power_col))

    if pollution:
        p7n_row = numpy.array(
            [r.pollution() for r in recipes.values()]
            + [0] * (matrix.shape[1] - len(recipes))
        )
        p7n_col = numpy.array([0] * matrix.shape[0] + [1])

        vector = numpy.append(vector, 0)

        matrix = numpy.vstack((matrix, p7n_row))
        matrix = numpy.column_stack((matrix, p7n_col))

    return matrix


def get_objective(matrix, recipes, priority):
    a = numpy.abs(matrix)
    a = a[a != 0]
    r = a.max() / a.min()

    return numpy.array(
        [
            next((r**i for i, names in enumerate(priority) if name in names), 0)
            for name in recipes.keys()
            # [*recipes.keys(), "power", "pollution"]
        ]
    )


def solve(aims, items, recipes, priority):
    vector = get_vector(aims, items)

    matrix = get_matrix(items, recipes)

    objective = get_objective(matrix, recipes, priority)

    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linprog.html
    sol = scipy.optimize.linprog(
        c=objective,
        A_ub=-matrix,
        b_ub=-vector,
        method="highs",
    )

    if not sol.success:
        exit(sol)

    return sol.x


def display(sol, recipes, extra, dp):
    print("machines:")
    for (k, v), c in zip(recipes.items(), sol):
        if c:
            s = f"{v.machine}>{k}: " f"{c:.{f'{dp}f' if k != 'crude-oil' else '0%'}}"

            match v.machine.energy_source:
                case "electric":
                    s += f", {format_power(v.machine.power_usage(c), dp)}"
                case "burner":
                    fc = v.machine.fuel_usage(c)
                    s += f", {fc:.{dp}f} {v.machine.fuel}"

            if v.machine.pollution(c):
                s += f", {v.machine.pollution(c):.{dp}f}p/m"
            if k == "crude-oil":
                s += f" = {c/pyj:.{dp}f} pumpjacks at {pyj:.0%} yield"
            print(s)

    tot_pow = sum(
        v.machine.power_usage(c)
        for v, c in zip(recipes.values(), sol)
        if v.machine.energy_source == "electric"
    )
    if tot_pow:
        print(f"tot pow: {format_power(tot_pow,dp)}")
    print(
        f"tot p7n: {sum(v.machine.pollution(c) for v, c in zip(recipes.values(), sol)):.{dp}f} p/m"
    )

    ingredients = collections.defaultdict(int)
    for recipe, c in zip(recipes.values(), sol):
        if c:
            for item, x in recipe.ingredients.items():
                ingredients[item] += c * x

    outputs = collections.defaultdict(int, {k: -v for k, v in ingredients.items()})
    for recipe, c in zip(recipes.values(), sol):
        if c:
            for item, x in recipe.results.items():
                outputs[item] += c * x
    outputs = {k: v for k, v in outputs.items() if not math.isclose(v, 0, abs_tol=1e-8)}

    print("inputs:")
    pseudos = extra["add"]["recipes"].keys()
    for k, v in ingredients.items():
        if k in pseudos:
            print(f"{k} {v:.{dp}f}")

    intermediates = {k: v for k, v in ingredients.items() if k not in pseudos}
    if intermediates:
        print("intermediates:")
        for k, v in intermediates.items():
            print(f"{k} {v:.{dp}f}")

    surplus = {k: v for k, v in outputs.items() if k not in aims.keys()}
    if surplus:
        print("surplus:")
        for k, v in surplus.items():
            print(f"{k} {v:.{dp}f}")

    print("outputs:")
    for k, v in outputs.items():
        if k in aims.keys():
            print(f"{k} {v:.{dp}f}")


if __name__ == "__main__":
    args = get_args()

    with open("extra.json", "r") as f:
        extra = json.load(f)

    with open("aims.json", "r") as f:
        target = json.load(f)

    aims = target["aim"]

    print("aim:", {k: v for k, v in aims.items() if v})

    items, recipes = items_and_recipes(extra, target)

    sol = solve(aims, items, recipes, target["priority"])

    display(sol, recipes, extra, target["decimal-places"])
