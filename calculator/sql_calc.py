#!/usr/bin/env python3

import argparse
import json
import logging
import math
import pathlib
import sqlite3
import sys
import time

import numpy
import scipy


logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout)


def pluck_factory(_, row):
    return row[0]


def took(timer, title="took"):
    dur = 1000 * (time.time() - timer)
    logger.info(f"{title} {dur:.0f}ms")
    return dur


def format_power(p, dp):
    u = "kW"

    if p > 1000:
        p /= 1000
        u = "MW"

    if p > 1000:
        p /= 1000
        u = "GW"

    return f"{p:.{dp}f}{u}"


def get_args():
    parser = argparse.ArgumentParser(description="Factorio calculator")
    di = "factorio-data.db"
    parser.add_argument(
        "-i",
        "--input",
        type=pathlib.Path,
        default=di,
        metavar="I",
        help=f"Path to SQLite3 database, default {di}",
    )
    parser.add_argument(
        "-t",
        "--tax",
        action="store_true",
        help="Enable recipe tax",
    )
    dl = "info"
    parser.add_argument(
        "-l",
        "--log-level",
        type=str,
        choices=("notset", "debug", "info", "warning", "error", "critical"),
        default=dl,
        help=f"Set log level, default {dl}",
    )
    da = "aims.json"
    parser.add_argument(
        "-a",
        "--aims",
        type=pathlib.Path,
        default=da,
        metavar="A",
        help=f"Set path for json file containing desired outputs in units of items per second, default {da}",
    )
    dd = 2
    parser.add_argument(
        "-d",
        "--decimal-places",
        type=int,
        default=dd,
        metavar="D",
        help=f"Set number of decimal places in outputs, default {dd}",
    )
    return parser.parse_args()


if __name__ == "__main__":
    start = time.time()

    args = get_args()
    dp = args.decimal_places

    logger.setLevel(args.log_level.upper())

    logger.info("loading data")
    timer_data = time.time()

    with open(args.aims, "r") as f:
        aims = json.load(f)

    try:
        con = sqlite3.connect(args.input)
        con.set_trace_callback(logger.debug)
        cur = con.cursor()
        cur.execute("PRAGMA foreign_keys = ON")

        cur.row_factory = pluck_factory

        pseudos = cur.execute(
            """
            SELECT i.name
            FROM item AS i,
                fundamental AS f ON f.id = i.id
            """
        ).fetchall()

        num_items = cur.execute("SELECT COUNT(id) FROM item").fetchone()
        num_recipes = cur.execute("SELECT COUNT(id) FROM recipe").fetchone()

        cur.execute(
            """
            CREATE TABLE aim (
                id INTEGER PRIMARY KEY REFERENCES item (id),
                amount REAL NOT NULL
            ) STRICT
            """
        )

        nza = [(k, v) for k, v in aims["aim"].items() if v]

        cur.executemany(
            """
            INSERT INTO aim (id, amount)
            VALUES ((SELECT id FROM item WHERE name = ?), ?)
            """,
            nza,
        )

        print("aims:")
        cur.row_factory = None
        for n, x in cur.execute(
            """
            SELECT i.name, a.amount
            FROM aim AS a,
                item AS i ON i.id = a.id
            """
        ).fetchall():
            print(n, x)

        t_load = took(timer_data)

        logger.info("pre-processing data for optimisation")
        timer_pre = time.time()

        cur.row_factory = pluck_factory
        vector = cur.execute(
            """
            SELECT IFNULL(a.amount, 0.0)
            FROM item AS i
            LEFT OUTER JOIN aim AS a ON i.id = a.id
            ORDER BY i.id
            """
        ).fetchall()
        vector = numpy.array(vector)

        logger.debug("vector:")
        logger.debug(vector)

        matrix = cur.execute(
            """
            SELECT m.crafting_speed / r.crafting_time * SUM(IFNULL(t.amount, 0))
            FROM item AS i
            FULL OUTER JOIN recipe AS r
            LEFT OUTER JOIN transput AS t ON t.recipe_id = r.id AND t.item_id = i.id
            INNER JOIN recipe_category AS c ON c.id = r.recipe_category_id
            INNER JOIN machine_for_recipe_category AS d ON d.recipe_category_id = c.id
            INNER JOIN machine AS m ON m.id = d.machine_id
            INNER JOIN active AS a ON a.id = d.id
            GROUP BY i.id, r.id
            ORDER BY i.id, r.id
            """
        ).fetchall()

        matrix = numpy.array(matrix).reshape((num_items, num_recipes))

        num_game = num_recipes - len(pseudos)

        # optionally introduce a "recipe tax":
        # a dummy cost on every (non-pseudo) recipe that is minimised
        # such that degenerate solutions using different sets of recipes
        # are broken to the one with the least recipes
        if args.tax:
            tax_row = numpy.array([1] * num_game + [0] * len(pseudos))
            tax_col = numpy.array([0] * num_items + [1])

            vector = numpy.append(vector, 0)

            matrix = numpy.vstack((matrix, tax_row))
            matrix = numpy.column_stack((matrix, tax_col))

        logger.debug("matrix:")
        logger.debug(matrix)

        # assume critical ratio r will be less than ratio between
        # largest and smallest absolute values in system
        a = numpy.abs(matrix)
        a = a[a != 0]
        r = a.max() / a.min()
        # then use r in objective function to rank input priorities

        # bump up lowest power from 0 to 1 if using tax so tax can use 0
        root = 1 if args.tax else 0

        objective = [0] * num_game + [
            r
            ** next(
                (
                    i
                    for i, names in enumerate(aims["priority"], start=root)
                    if name in names
                ),
                root,
            )
            for name in pseudos
        ]

        if args.tax:
            objective += [1]

        logger.debug("objective:")
        logger.debug(objective)

        t_pre = took(timer_pre)

        logger.info("solving")
        timer_sol = time.time()

        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.linprog.html
        sol = scipy.optimize.linprog(
            c=objective,
            A_ub=-matrix,
            b_ub=-vector,
        )

        logger.debug(sol)

        if not sol.success:
            raise Exception("Solver failed")

        t_solve = took(timer_sol)

        logger.info("post-processing result")
        timer_post = time.time()

        cur.execute(
            """
            CREATE TABLE recipe_coefficients (
                id INTEGER PRIMARY KEY REFERENCES recipe (id),
                coeff REAL NOT NULL
            ) STRICT
            """
        )

        cur.executemany(
            """
            INSERT INTO recipe_coefficients (coeff)
            VALUES (?)
            """,
            [(x,) for x in sol.x],
        )

        cur.row_factory = None
        recipe_coeffs = cur.execute(
            """
            SELECT
                m.name AS machine,
                c.coeff AS number,
                r.name AS recipe,
                c.coeff * m.full_power_draw + CEIL(c.coeff) * m.drain AS power,  -- missing drain
                c.coeff * m.pollution AS pollution
            FROM
                recipe AS r,
                recipe_coefficients AS c ON c.id = r.id,
                recipe_category AS b ON b.id = r.recipe_category_id,
                machine_for_recipe_category AS d ON d.recipe_category_id = b.id,
                machine AS m ON m.id = d.machine_id,
                active AS a ON a.id = d.id
            WHERE c.coeff
            """
        ).fetchall()

        print("machines:")
        for machine, num, recipe, power, p7n in recipe_coeffs:
            print(
                f"{machine} > {recipe}: {num:.{dp}f} | {format_power(power, dp)} | {p7n:.{dp}f} p7n"
            )

        cur.execute(
            """
            CREATE TABLE result_coefficients (
                id INTEGER NOT NULL REFERENCES item (id),
                number REAL NOT NULL
            ) STRICT
            """
        )

        cur.execute(
            """
            INSERT INTO result_coefficients (id, number)
            SELECT
                i.id,
                SUM(m.crafting_speed / r.crafting_time * t.amount * c.coeff)
            FROM
                recipe AS r,
                recipe_coefficients AS c ON c.id = r.id,
                transput AS t ON t.recipe_id = r.id,
                item AS i ON i.id = t.item_id,
                recipe_category AS b ON b.id = r.recipe_category_id,
                machine_for_recipe_category AS d ON d.recipe_category_id = b.id,
                machine AS m ON m.id = d.machine_id,
                active AS a ON a.id = d.id
            WHERE c.coeff AND t.amount > 0
            GROUP BY i.id
            """
        ).fetchall()

        cur.execute(
            """
            CREATE TABLE outputs (
                id INTEGER PRIMARY KEY REFERENCES item (id),
                number REAL NOT NULL
            ) STRICT
            """
        )

        cur.execute(
            """
            INSERT INTO outputs (id, number)
            SELECT
                i.id,
                SUM(m.crafting_speed / r.crafting_time * t.amount * c.coeff)
            FROM
                recipe AS r,
                recipe_coefficients AS c ON c.id = r.id,
                transput AS t ON t.recipe_id = r.id,
                item AS i ON i.id = t.item_id,
                recipe_category AS b ON b.id = r.recipe_category_id,
                machine_for_recipe_category AS d ON d.recipe_category_id = b.id,
                machine AS m ON m.id = d.machine_id,
                active AS a ON a.id = d.id
            WHERE c.coeff
            GROUP BY i.id
            """
        ).fetchall()

        final = cur.execute(
            """
            WITH a AS (
                SELECT
                    i.name,
                    c.number - o.number AS intermediate,
                    o.number AS output,
                    f.id IS NULL AS solid
                FROM
                    item AS i,
                    result_coefficients AS c ON i.id = c.id,
                    outputs AS o ON i.id = o.id
                LEFT OUTER JOIN fluid AS f ON f.id = i.id
            )
            SELECT
                name, intermediate, output, solid, intermediate / 15 AS belts
            FROM a
            """
        ).fetchall()

        cur.row_factory = pluck_factory
        fundamentals = cur.execute(
            "SELECT i.name FROM item AS i, fundamental AS f ON i.id = f.id"
        ).fetchall()

        # should have done this inside sql
        print("items:")
        for item, inter, out, solid, belts in final:
            st = f"{item}:"
            if inter:
                st += f" {inter:.{dp}f}"
                if solid:
                    st += f" = {belts:.{dp}f} y-belts"
                if item in fundamentals:
                    st += " (input)"
                else:
                    st += " (inter)"
            if not math.isclose(out, 0, abs_tol=1e-8):
                op = item in aims["aim"].keys()
                if not op:
                    st += " |"
                st += f" {out:.{dp}f}"
                if op:
                    st += " (output)"
                else:
                    st += " (surplus)"
            print(st)

        t_post = took(timer_post)

    finally:
        cur.execute("DROP TABLE aim")
        cur.execute("DROP TABLE recipe_coefficients")
        cur.execute("DROP TABLE result_coefficients")
        cur.execute("DROP TABLE outputs")

        con.commit()
        con.close()

    t_tot = took(start, "total:")

    logger.info("time distribution:")
    logger.info(
        f"load: {t_load/t_tot:.0%}, pre: {t_pre/t_tot:.0%}, "
        f"solve: {t_solve/t_tot:.0%}, post: {t_post/t_tot:.0%}"
    )
